import React from 'react';
import "./todo-add.css"

class TodoAdd extends React.Component {
  state = {
    text: ''
  }

  render() {
    return (
      <form className="item-add-form d-flex" onSubmit={(event) => {
        const {onAdd} = this.props;

        onAdd(this.state.text)
        this.setState({text: ''})

        event.preventDefault()
      }}>
        <input
          onChange={(event) => this.setState(
              {text: event.target.value}
            )
          }
          type='text'
          value={this.state.text}
          placeholder="what to do?"
          className="form-control"
        />
        <button className="btn btn-primary">
            Add Item
        </button>
      </form>
    )
  }
}

export default TodoAdd;
